# Power Plants Frontend Introduction

As a user, once you access the website you will be able to see the states having annual net generation, and their percentages which is the top will be highlighted with darker green and the last will be brighter.

You can hover on each state to see its statistics, and for more details about the plants you can click on the state to see it's plants locations and their information and auto zoom the map for better user experience.

On other hand, if you want to get top N plants you can use the form on the top left, choose the number and the state.
Once you got the results, new bar chart will appear showing the stats for each plant and the map will updated automatically.

If you decided to go to specific plant location you can press on the bar :).


This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.3.9.

## Online Demo
[Power Plants](https://power-plants-frontend.herokuapp.com) delpoyed on Heroku.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
