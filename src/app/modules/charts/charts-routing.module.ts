import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PowerPlantsMapComponent } from './pages/power-plants-map/power-plants-map.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'power-plants',
    pathMatch: 'full'
  },
  {
    path: 'power-plants',
    component: PowerPlantsMapComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChartsRoutingModule { }
