import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PlantsDetailsResultModel } from '../../models/plants-details-result.model';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html'
})
export class BarChartComponent implements OnInit {
  @Output('barClicked') barClicked = new EventEmitter<any>();
  @Input('data') data!: PlantsDetailsResultModel[];
  chartOption: any;
  constructor() { }

  ngOnInit(): void {
    if (this.data)
      this.setChartOptions(this.data);
  }
  onChartClick(e: any) {
    this.barClicked.emit(e);
  }
  setChartOptions(plants: PlantsDetailsResultModel[]) {
    this.data = plants;
    // For Bar Sorting View
    const reversedData = plants.reverse();
    this.chartOption = {
      xAxis: {
        type: 'category',
        data: reversedData.flatMap((plant) => { return plant.name }),
        show: false
      },
      tooltip: {
        trigger: 'item',
        position: 'right',
      },
      yAxis: {
        type: 'value',
        position: 'bottom',
        axisLabel: {
          fontSize: 8,
        }
      },
      series: [
        {
          data: reversedData.flatMap((plant) => { return plant.annualNetGeneration }),
          type: 'bar',
          label: {
            show: reversedData.length <= 15 ? true : false,
            position: 'insideBottom',
            distance: 15,
            align: 'left',
            verticalAlign: 'middle',
            rotate: 90,
            formatter: '{b}',
            rich: {
              name: {}
            }
          },
          color: '#198754'
        }
      ]
    };
  }
}
