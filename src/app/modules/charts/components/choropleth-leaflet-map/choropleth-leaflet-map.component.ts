import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef, Component, ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
import * as L from 'leaflet';
import { environment } from 'src/environments/environment';
import { PlantsDetailsResultModel } from '../../models/plants-details-result.model';
import { StatesPlantsResultModel } from '../../models/states-plants-result.model';
import * as chroma from "chroma-js";
import { geoJSON, tileLayer, circleMarker, LeafletMouseEvent } from 'leaflet';
import { RangeValue } from '../../models/range-value.model';
import { SearchPlantModel } from '../../models/search-plant.model';
import { PlantsService } from '../../services/plants.service';
import { EChartsOption, ECharts } from 'echarts';
import { BarChartComponent } from '../bar-chart/bar-chart.component';
type BarLabelOption = NonNullable<echarts.BarSeriesOption['label']>;
@Component({
  selector: 'app-choropleth-leaflet-map',
  templateUrl: './choropleth-leaflet-map.component.html'
})
export class ChoroplethLeafletMapComponent implements OnInit {
  @ViewChild('markerPopup') markerPopup!: ElementRef;
  @ViewChild('statePopup') statePopup!: ElementRef;
  @ViewChild('barChart') barChart!: BarChartComponent;
  options: any;
  map!: L.Map;
  geoJsonData: any;
  statesData: StatesPlantsResultModel[] = [];
  plants: PlantsDetailsResultModel[] = [];
  currentHoveredPlant: PlantsDetailsResultModel | undefined;
  currentHoveredState: StatesPlantsResultModel | undefined;
  currentHoveredStateTitle!: string;
  geoJsonElement!: L.GeoJSON;
  rangeValues!: RangeValue;
  showStatePopup: boolean = false;
  showMarkerPopup: boolean = false;
  plantsMarkers: L.CircleMarker[] = [];
  searchModel = new SearchPlantModel();
  chartOption: EChartsOption = {};
  submitted: boolean = false;

  constructor(private plantService: PlantsService, private zone: NgZone) {
    this.setStateStyle = this.setStateStyle.bind(this);
    this.setHighlightFeature = this.setHighlightFeature.bind(this);
    this.setZoomToFeature = this.setZoomToFeature.bind(this);
    this.resetHighlight = this.resetHighlight.bind(this);
    this.markerMouseOver = this.markerMouseOver.bind(this);
    this.markerMouseOut = this.markerMouseOut.bind(this);
    this.markerClicked = this.markerClicked.bind(this);
  }

  ngOnInit() {
    this.init();
  }
  private init() {
    this.getStates()
  }

  // Getters
  private getStates() {
    this.plantService.getStates().subscribe((res: StatesPlantsResultModel[]) => {
      this.statesData = res;
      this.setRangeValues();
      this.setOptions();
      this.setGeoJson();
    })
  }

  private getPlantsByState(state: string) {
    this.plantService.getStatePlants(state).subscribe((plants: PlantsDetailsResultModel[]) => {
      this.plants = plants;
      this.setRenderPlantsMarkers();
      this.showMarkerPopup = true;
    })
  }

  getSearchResult() {
    if (this.searchModel.numberOfPlants > 0) {
      this.submitted = true;
      this.plantService.getTopPlants(this.searchModel).subscribe((plants: PlantsDetailsResultModel[]) => {
        this.clearMarkers();
        this.resetMapZoom();
        this.plants = plants;
        this.setRenderPlantsMarkers();
        this.barChart?.setChartOptions(plants);
        this.showMarkerPopup = true;
        if (this.searchModel.state != null && this.searchModel.state != '') {
          const group = L.featureGroup(this.plantsMarkers);
          this.map.flyToBounds(group.getBounds(), { maxZoom: 7 });
        }
      })
    }
  }
  private getColor(state: string) {
    const stateAnnualNetGeneration = this.statesData.find(x => x.state == state)?.annualNetGeneration ?? 0;
    if (stateAnnualNetGeneration <= this.rangeValues.maxValue && stateAnnualNetGeneration >= this.rangeValues.firstRangeValue) {
      return '#006837';
    }
    else if (stateAnnualNetGeneration < this.rangeValues.firstRangeValue && stateAnnualNetGeneration >= this.rangeValues.secondRangeValue) {
      return '#78c679';
    }
    else if (stateAnnualNetGeneration < this.rangeValues.secondRangeValue && stateAnnualNetGeneration >= this.rangeValues.thirdRangeValue) {
      return '#c2e699';
    }
    else {
      return '#ffffcc';
    }
  }
  //Setters
  private setRenderPlantsMarkers() {
    for (let plant of this.plants) {
      if (plant.latitude && plant.longitude) {
        const lat = parseFloat(plant.latitude);
        const lng = parseFloat(plant.longitude);
        const marker = circleMarker([lat, lng], { color: '#34aeeb', pane: 'locationMarker' });
        // Check if marker is exists
        if (!this.plantsMarkers.some(x => x.getLatLng().lat == lat && x.getLatLng().lng == lng)) {
          this.setMouseOverEventListener(marker, plant);
          this.setMouseOutEventListener(marker);
          this.setClickEventListener(marker);
          this.setRenderMarker(marker);
        }
      }
    }
  }

  private setRenderMarker(marker: L.CircleMarker<any>) {
    this.zone.run(() => {
      marker.setStyle({ opacity: 0.3 });
      marker.bringToFront();
      marker.bindTooltip(this.markerPopup.nativeElement, { pane: 'locationTooltip', direction: 'top' });
      marker.addTo(this.map);
      this.plantsMarkers.push(marker);
    });
  }
  private setStateStyle(feature: any) {
    const color = this.getColor(feature.properties.alpha_code);
    return {
      fillColor: color,
      weight: 2,
      opacity: 1,
      color: 'white',
      dashArray: '3',
      fillOpacity: 0.7
    };
  }
  private setPanes(map: L.Map) {
    const plantMarkerElement = map.createPane("locationMarker");
    const markerTooltipElement = map.createPane("locationTooltip");
    plantMarkerElement.style.zIndex = '999';
    markerTooltipElement.style.zIndex = '1000';
  }
  private setOptions() {
    this.options = {
      layers: [
        tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: '...' }),
      ],
      zoom: 4,
      center: L.latLng(37.8, -96)
    }
  }

  private setGeoJson() {
    this.plantService.getUsStatesGeoJson().subscribe((res: any) => {
      this.geoJsonData = res;
      this.geoJsonElement = geoJSON(res, {
        style: this.setStateStyle,
        onEachFeature: this.onEachFeature
      })
    })
  }

  private setRangeValues() {
    const sum = this.statesData.reduce((accumulator, object) => {
      return accumulator + object.annualNetGeneration;
    }, 0);
    const maxValue = this.statesData[0].annualNetGeneration;
    const minValue = this.statesData[this.statesData.length - 1].annualNetGeneration;
    const avg = sum / this.statesData.length;
    this.rangeValues = {
      maxValue: maxValue,
      minValue: minValue,
      avg: avg,
      firstRangeValue: (maxValue + avg) / 2,
      secondRangeValue: avg,
      thirdRangeValue: (avg + minValue) / 2
    }
  }
  private setHighlightFeature(e: any) {
    this.zone.run(() => {
      this.currentHoveredState = this.statesData.find(x => x.state == e.target.feature.properties.alpha_code);
      this.currentHoveredStateTitle = e.target.feature.properties.name
      this.showStatePopup = true;
    })

    const layer = e.target;
    layer.setStyle({
      fillColor: chroma(this.getColor(e.target.feature.properties.alpha_code)).darken(0.8).hex(),
      dashArray: "",
      fillOpacity: 0.9,
    });

    if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
      layer.bringToFront();
    }
  }

  private setZoomToFeature(e: any) {
    this.map.fitBounds(e.target.getBounds());
    this.getPlantsByState(e.target.feature.properties.alpha_code)
  }

  private setMouseOutEventListener(marker: L.CircleMarker<any>) {
    marker.on({
      mouseout: this.markerMouseOut,
    });
  }

  private setClickEventListener(marker: L.CircleMarker<any>) {
    marker.on({
      click: this.markerClicked,
    });
  }

  private setMouseOverEventListener(marker: L.CircleMarker<any>, plant: PlantsDetailsResultModel) {
    marker.on('mouseover', (e) => {
      this.markerMouseOver(e, plant);
    });
  }
  // Resets / Clearers
  private clearMarkers() {
    for (let marker of this.plantsMarkers) {
      marker.removeFrom(this.map);
    }
    this.plantsMarkers = [];
  }

  private resetMapZoom() {
    this.map.setView([this.map.getCenter().lat, this.map.getCenter().lng], 4);
  }

  reset() {
    this.searchModel = new SearchPlantModel();
    this.clearMarkers();
    this.resetMapZoom();
    this.barChart?.setChartOptions([]);
    this.showMarkerPopup = false;
  }

  private resetHighlight(e: any) {
    this.geoJsonElement.resetStyle(e.target);
  }

  //Events
  onChartClick(e: any) {
    const clickedPlant = this.plants.find(x => x.name == e.name && x.annualNetGeneration == e.data);
    if (clickedPlant) {
      const targetMarker = this.plantsMarkers.find(x => x.getLatLng().lat == parseFloat(clickedPlant.latitude) && x.getLatLng().lng == parseFloat(clickedPlant.longitude))
      if (targetMarker) {
        const group = L.featureGroup([targetMarker]);
        this.map.fitBounds(group.getBounds());
      }
    }
  }

  private markerMouseOut(e: LeafletMouseEvent) {
    const targetMarker = this.plantsMarkers.find(x => x.getLatLng() == e.latlng);
    targetMarker?.setStyle({ opacity: 0.3 })
  }

  private markerClicked(e: LeafletMouseEvent) {
    const targetMarker = this.plantsMarkers.find(x => x.getLatLng() == e.latlng);
    if (targetMarker) {
      var group = L.featureGroup([targetMarker]);
      this.map.fitBounds(group.getBounds());
    }
  }

  private markerMouseOver(e: LeafletMouseEvent, plant: any) {
    const targetMarker = this.plantsMarkers.find(x => x.getLatLng() == e.latlng);
    targetMarker?.setStyle({ opacity: 1 })
    this.zone.run(() => {
      this.currentHoveredPlant = plant;
    })
  }

  private onEachFeature = (feature: any, layer: L.Layer) => {
    layer.on({
      mouseover: this.setHighlightFeature,
      mouseout: this.resetHighlight,
      click: this.setZoomToFeature
    });
    layer.bindTooltip(this.statePopup.nativeElement, { pane: 'locationTooltip', direction: 'top' }).openTooltip();
  };

  onMapReady(map: L.Map) {
    this.map = map;
    this.setPanes(map);
    this.map.zoomControl.setPosition('topright')
  }

}
