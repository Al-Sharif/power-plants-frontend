export class StatesPlantsResultModel {
    state!: string;
    totalPlants!: number;
    annualNetGeneration!: number;
    percentage!: number;
}