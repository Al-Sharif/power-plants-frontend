export class PlantsDetailsResultModel {
    year!: number;
    state!: string;
    name!: string;
    owner!: string;
    balancingAuthorityName!: string;
    countyName!: string;
    latitude!: string;
    longitude!: string;
    unitsCount!: number;
    generatorsCount!: number;
    annualNetGeneration!: number;
}