export class RangeValue {
    maxValue!: number;
    minValue!: number;
    avg!: number;
    firstRangeValue!: number;
    secondRangeValue!: number;
    thirdRangeValue!: number;
}