import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChartsRoutingModule } from './charts-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { PowerPlantsMapComponent } from './pages/power-plants-map/power-plants-map.component';
import { ChoroplethLeafletMapComponent } from './components/choropleth-leaflet-map/choropleth-leaflet-map.component';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { FormsModule } from '@angular/forms';
import { PlantsService } from './services/plants.service';
import { NgxEchartsModule } from 'ngx-echarts';
import { BarChartComponent } from './components/bar-chart/bar-chart.component';
@NgModule({
  declarations: [
    PowerPlantsMapComponent,
    ChoroplethLeafletMapComponent,
    BarChartComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    ChartsRoutingModule,
    LeafletModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts'),
    }),
    FormsModule
  ],
  providers: [PlantsService],

})
export class ChartsModule { }
