import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { PlantsDetailsResultModel } from '../models/plants-details-result.model';
import { SearchPlantModel } from '../models/search-plant.model';
import { StatesPlantsResultModel } from '../models/states-plants-result.model';

@Injectable({
  providedIn: 'root'
})
export class PlantsService {
  private controllerPath = `${environment.baseUrl}${environment.chartModulePath}/${environment.plantsControllerPath}`;
  constructor(private httpClient: HttpClient) { }
  
  getUsStatesGeoJson() {
    return this.httpClient.get(environment.usStatesPath);
  }

  getStates() {
    return this.httpClient.get<StatesPlantsResultModel[]>(this.controllerPath)
  }

  getStatePlants(state: string) {
    return this.httpClient.get<PlantsDetailsResultModel[]>(`${this.controllerPath}/state-plants/${state}`)
  }

  getTopPlants(model: SearchPlantModel) {
    return this.httpClient.get<PlantsDetailsResultModel[]>(`${this.controllerPath}/top/?numberOfPlants=${model.numberOfPlants}&state=${model.state}`)
  }
}
