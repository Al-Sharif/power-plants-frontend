export const environment = {
  production: true,
  usStatesPath: '/assets/data/geo-us-states.json',
  baseUrl: 'https://power-plants-backend.herokuapp.com/',
  chartModulePath: 'charts',
  plantsControllerPath: 'plants'
};
